var isVPS=false;
//Prepare the JQueryUI components
$(function () {
	//Create variables for the progress bars
    var progressbarGenSigningKey = $("#progressbarGenSigningKey"),
        progressbarGenCryptoKey = $("#progressbarGenCryptoKey"),
        progressbarGenBallots = $("#progressbarGenBallots"),
        progressbarAuditBallots = $("#progressbarAuditBallots");
    var progressLabelGenKey = $(".progress-label-genSigningKey"),
        progressLabelGenCryptoKey = $(".progress-label-genCryptoKey"),
        progressLabelGenBallots = $(".progress-label-genBallots"),
        progressLabelAuditBallots = $(".progress-label-auditBallots");

	//Prepare Generate Signing Key Progress Bar
    progressbarGenSigningKey.progressbar({
        value: false,
        change: function () {
            progressLabelGenKey.text(progressbarGenSigningKey.progressbar("value") + "%");
        },
        complete: function () {
            //progressLabelGenKey.text( "Complete!" );
        }
    });

	//Prepare Generate Crypto Key Progress Bar
    progressbarGenCryptoKey.progressbar({
        value: false,
        change: function () {
            progressLabelGenCryptoKey.text(progressbarGenCryptoKey.progressbar("value") + "%");
        },
        complete: function () {
            //progressLabelGenCryptoKey.text( "Complete!" );
        }
    });

	//Prepare Generate Ballots Progress Bar
    progressbarGenBallots.progressbar({
        value: false,
        change: function () {
            progressLabelGenBallots.text(progressbarGenBallots.progressbar("value") + "%");
        },
        complete: function () {
            //progressLabelGenBallots.text( "Complete!" );
        }
    });

	//Prepare Audit Ballots Progress Bar
    progressbarAuditBallots.progressbar({
        value: false,
        change: function () {
            progressLabelAuditBallots.text(progressbarAuditBallots.progressbar("value") + "%");
        },
        complete: function () {
            //progressLabelAuditBallots.text( "Complete!" );
        }
    });

	//Set the progress bar values to zero
    progressbarGenSigningKey.progressbar("value", 0);
    progressbarGenCryptoKey.progressbar("value", 0);
    progressbarGenBallots.progressbar("value", 0);
    progressbarAuditBallots.progressbar("value", 0);
});



//Variable to hold reference to websocket
var exampleSocket;
var localPort;
function updateAndConnect(){
	$.getJSON('js/localport.json', function(data) {
		localPort = data.port;
		connect();
	});
}
//Connects to the websocket - localPort is set in localport.js that is dynamically updated by the server
function connect() {
    exampleSocket = new WebSocket("ws://localhost:" + localPort);
    exampleSocket.onclose = function (evt) {
        onClose(evt);
    };
    exampleSocket.onerror = function (evt) {
        onError(evt);
    };
    exampleSocket.onmessage = function (event) {
        processMessage(event);
    };
    exampleSocket.onopen = function (event) {
        document.getElementById("status").innerHTML = "Connected";
        getType();
        getStatus();
        
    };
}

//Requests the signing key from the server
function genSigningKey() {
    exampleSocket.send("{'type':'genSigningKey'}");
}

//Requests the CSR from the server
function getCSR() {
    exampleSocket.send("{'type':'getCSR'}");
}

//Gets the current status - where we are in the bootstrap process
function getStatus() {
    exampleSocket.send("{'type':'getStatus'}");
}


//Gets the type of client - what should we display
function getType() {
    exampleSocket.send("{'type':'getType'}");
}

//Asks the server to generate the Crypto Key
function genCryptoKey() {
    exampleSocket.send("{'type':'genCryptoKey'}");
}

//Asks the server to start generating ballots
function genBallots() {
    exampleSocket.send("{'type':'genBallots'}");
}
function importCerts(){
	var req = {};
	req.type ="importCerts";
	//req.cert= document.getElementById("SigningCert").value;
	req.sslcert= document.getElementById("SSLCert").value;
	//if(req.cert.length>0 && req.sslcert.length>0){
	if(req.sslcert.length>0){
	 exampleSocket.send(JSON.stringify(req));
	}else{
	 window.alert("Both the Signing Certificate and SSL Certificate must be set");
	}
}

//Called when a message is received on the WebSocket. Works out what the message is related to and updates accordingly
function processMessage(event) {
	//We should always get a JSONMessage back
    var msg = JSON.parse(event.data);
    if (msg.type == "progressUpdate") {
    	//Indicates an update for one of the progress bars, find out which one and update it
        if (msg.progressID == "genSigningKey") {
            $("#progressbarGenSigningKey").progressbar("value", msg.value + 0.0);
        } else if (msg.progressID == "genCryptoKey") {
            $("#progressbarGenCryptoKey").progressbar("value", msg.value + 0.0);
        } else if (msg.progressID == "genBallots") {
        	//Because genBallots is a two step process (generate and audit) we only use one id and just switch to updating the
        	//audit progress bar once we have finished the generation - an audit shouldn't occur if the generation does not finish
            if ($("#progressbarGenBallots").progressbar("value") != 100) {
                $("#progressbarGenBallots").progressbar("value", msg.value + 0.0);
            } else {
                $("#progressbarAuditBallots").progressbar("value", msg.value + 0.0);
            }
        } else if (msg.progressID == "filesProgress") {
            $("#progressbarFiles").progressbar("value", msg.value + 0.0);
        }
    } else if (msg.type == "status") {
    	//This is a status update, normally returned after a getStatus call
    	//Show the disable divs on all items 
        document.getElementById("keyGenDisable").style.display = "block";
        document.getElementById("importCertDisable").style.display = "block";
        if(isVPS){
        	document.getElementById("genCryptoKeyDisable").style.display = "block";
        	document.getElementById("genBallotsDisable").style.display = "block";
        }
        if (msg.status == "Initialised") {
        	//If we are initialised there is nothing else to do 
            document.getElementById("nextStep").innerHTML = "System is initialised - Ready for use";
        } else if (msg.status == "SigningKeyStore") {
        	//Next step is signing key generation
            document.getElementById("nextStep").innerHTML = "Next Step: Create Signing Key and CSR";
            document.getElementById("keyGenDisable").style.display = "none";
        } else if (msg.status == "CertificateLocation") {
        	//Next step is to import certificate, want to allow getCSR
            document.getElementById("nextStep").innerHTML = "Next Step: Import Certificate";
            document.getElementById("keyGenDisable").style.display = "none";
            document.getElementById("importCertDisable").style.display = "none";
            document.getElementById("keyGenButton").style.display = "none";
            document.getElementById("getCSRButton").style.display = "block";
        } else if (msg.status == "EncryptionKeyStore") {
        	//Next step is to generate the encryption key
            document.getElementById("nextStep").innerHTML = "Next Step: Generate Crypto Key Pair";
            document.getElementById("genCryptoKeyDisable").style.display = "none";
        } else if (msg.status == "BallotsGenerated") {
        	//Next step is to generate the ballots
            document.getElementById("nextStep").innerHTML = "Next Step: Generate Ballots";
            document.getElementById("genBallotsDisable").style.display = "none";
        } 

    } else if(msg.type=="clientType"){
    	
        if(msg.clientType=="EVM" || msg.clientType=="CANCEL"){
        	isVPS=false;
        	document.getElementById("genCryptoKeyDisable").style.display = "none";
        	document.getElementById("genBallotsDisable").style.display = "none";
        	document.getElementById("genCryptoKeyDiv").style.display = "none";
        	document.getElementById("genBallotsDiv").style.display = "none";
        }else if(msg.clientType=="VPS"){
        	isVPS=true;
        	
        }
    } else if (msg.type == "finished") {
    	//Sent when a task has finished. In the case of generating the Signing Key we now want to enable the getCSR button
        if (msg.taskID == "genSigningKey") {
            document.getElementById("keyGenButton").style.display = "none";
            document.getElementById("getCSRButton").style.display = "block";
            getStatus();
        } else {
        	//otherwise we just update the status
            getStatus();
        }
    } else if (msg.type == "CSRString") {
    	//CSRString is the response following a request to get the CSR. This downloads it makes it file download locally
        openFile(msg.csr, 'application/octet-stream',false,'hiddenLink');
        openFile(msg.sslcsr, 'application/octet-stream',false,'hiddenLinkSSL');
    }else if (msg.type == "Error"){
    	alert("An error has occured: " + msg.msg);    	
    }
}

//Function that is called when the websocket closes
function onClose(evt)
{
    document.getElementById("status").innerHTML = "Disconnected";
}

//Function that is called when there is an error on the websocket
function onError(evt) {
    console.log(evt.data);
}

//Positions one element over the top of another. used to disable options by placing a div over the top
function positionOver(target, parent) {

    $(target).position({
        my: "left top",
        at: "left top",
        of: parent
    });
}
//When first loading position the disabling divs over all components
$(function () {
    positionOver("#keyGenDisable", "#keyGenDiv");
    positionOver("#importCertDisable", "#importCertDiv");
    positionOver("#genCryptoKeyDisable", "#genCryptoKeyDiv");
    positionOver("#genBallotsDisable", "#genBallotsDiv");
});

//Utility method for creating a file download from local content received via the WebSocekt
function openFile(textToEncode, contentType, newWindow, dlLink) {
    // For window.btoa (base64) polyfills, see 
    // https://github.com/Modernizr/Modernizr/wiki/HTML5-Cross-browser-Polyfills
    var encodedText = window.btoa(textToEncode);
    var dataURL = 'data:' + contentType + ';base64,' + encodedText;
    if (newWindow) { // Not useful for application/octet-stream type
        window.open(dataURL); // To open in a new tab/window
    } else {
    	console.log("here");
        document.getElementById(dlLink).href = dataURL;
        document.getElementById(dlLink).click();
    }
}