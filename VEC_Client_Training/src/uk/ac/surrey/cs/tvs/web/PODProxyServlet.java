/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyException;
import uk.ac.surrey.cs.tvs.client.TrainingClient;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle ballot reduction.
 * 
 * @author Chris Culnane
 * 
 * @version $Revision: 1.0 $
 */
public class PODProxyServlet implements NanoServlet {

  /**
   * TVSKeyStore for holding the client keys
   */
  private TVSKeyStore         clientKeyStore;

  /**
   * Configuration file for the district information.
   */
  private JSONObject          districtConf;

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(PODProxyServlet.class);

  /**
   * String to hold the schema in
   */
  private String              schema;

  /**
   * Path to the schema that covers messages received by this proxy
   */
  private static String       SCHEMA_PATH = "/sdcard/trainingmode/schemas/podproxyschema.json";

  private TrainingClient      trainingClient;
  private TrainingBallotDB    ballotDB;

  /**
   * Creates a new PODProxyServlet that references the configFile and districtConfig as specified. This will handle Print on Demand
   * requests from the client HTML.
   * 
   * 
   * @param districtConfFile
   *          path to district configuration file - number of candidates in each race
   * @param client
   *          Client
   * @throws JSONIOException
   * @throws CryptoIOException
   * @throws NoSuchAlgorithmException
   */
  public PODProxyServlet(TrainingClient client, String districtConfFile) throws JSONIOException, CryptoIOException,
      NoSuchAlgorithmException {
    super();
    this.trainingClient = client;
    logger.info("Loading PODProxyServlet");

    this.districtConf = IOUtils.readJSONObjectFromFile(districtConfFile);
    this.ballotDB = new TrainingBallotDB(this.districtConf);
    this.schema = JSONUtils.loadSchema(PODProxyServlet.SCHEMA_PATH);
    logger.info("Finished loading PODProxyServlet");
  }

  /**
   * Process and incoming message string, which should be in JSON format and with the relevant fields.
   * 
   * @param message
   *          JSON String of a message
   * 
   * 
   * @return JSONObject response
   * @throws ProxyException
   * 
   */
  private JSONObject processMessage(String message) throws ProxyException {
    try {
      // We know the type is pod because the schema restricts it

      // Load the message object
      JSONObject messageSent = new JSONObject(message);

            
      // Open the ballot list and ballotDB files
      // JSONArray ballots = IOUtils.readJSONArrayFromFile(this.conf.getStringParameter(BallotGenConfig.BALLOT_LIST));
      // JSONObject ballotDB = IOUtils.readJSONObjectFromFile(this.conf.getStringParameter(BallotGenConfig.BALLOT_DB));

      String id = this.trainingClient.getClientConf().getStringParameter(ConfigFiles.ClientConfig.ID);
      String serialNo = this.generateRandomSerialNo(id);
      JSONObject ballot = this.ballotDB.getBallot(serialNo, messageSent.getString(ClientConstants.UIPODMessage.DISTRICT));
      String sig = TrainingMBB.getInstance().signPODMessage(ballot);
      ballot.put("sigs", sig);
      // Return the response
      return ballot;

    }
    catch (JSONException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
  }

  private String generateRandomSerialNo(String id) throws ProxyException {
    for (int i = 0; i < 100; i++) {
      SecureRandom randIndex = new SecureRandom();
      int index = randIndex.nextInt(999) + 1;
      String serialNo = id + ":" + index;
      if (!TrainingMBB.getInstance().serialNoUsed(serialNo)) {
        return serialNo;
      }
    }
    throw new ProxyException("No ballots available");
  }

  /**
   * Runs the servlet.
   * 
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        if (JSONUtils.validateSchema(this.schema, params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, this.processMessage(
              params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }
      catch (ProxyException e) {
        logger.error("Exception whilst processing message {}", params.get(ClientConstants.REQUEST_MESSAGE_PARAM), e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
    }
    else {
      logger.warn("Missing msg parameter from request");
      return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT,
          "Missing msg parameter - no message to process");
    }
  }

 
}
