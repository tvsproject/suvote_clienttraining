/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;

/**
 * Utility class to create training mode keys. This will generate a keystore with all the necessary dummy keys.
 * 
 * @author Chris Culnane
 * 
 */
public class SetupTrainingKeys {

  /**
   * Default constructor, not used, all processing is done in the main method
   */
  public SetupTrainingKeys() {

  }

  /**
   * Main method to create training mode keys
   * 
   * @param args
   *          String arrays of args, none expected
   * @throws KeyStoreException
   * @throws IOException
   */
  public static void main(String[] args) throws KeyStoreException, IOException {
    BLSKeyPair mbbKP = BLSKeyPair.generateKeyPair();
    BLSKeyPair evmKP = BLSKeyPair.generateKeyPair();
    BLSKeyPair vpsKP = BLSKeyPair.generateKeyPair();
    BLSKeyPair cancelAuthKP = BLSKeyPair.generateKeyPair();
    TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    tvsKeyStore.addBLSKeyPair(mbbKP, "MBB");
    tvsKeyStore.addBLSKeyPair(evmKP, "EVM");
    tvsKeyStore.addBLSKeyPair(vpsKP, "VPS");
    tvsKeyStore.addBLSKeyPair(cancelAuthKP, "CancelAuth");
    tvsKeyStore.store("./demo/TestDeviceOne/trainingKeystore.bks");
  }

}
