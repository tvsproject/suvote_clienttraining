/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.security.KeyStore;
import java.security.KeyStoreException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;

/**
 * Training mode Keystore to provide access to the dummy keys used by training mode. This provides a singleton instance to access
 * this Keystore from anywhere within the trainingmode code
 * 
 * @author Chris Culnane
 * 
 */
public class TrainingKeyStore {

  /**
   * Single instance
   */
  private static final TrainingKeyStore _trainingKeyStore = new TrainingKeyStore();

  /**
   * Underlying TVSKeyStore with training mode keys
   */
  private TVSKeyStore                   keyStore;
  /**
   * Logger
   */
  private static final Logger           logger            = LoggerFactory.getLogger(TrainingKeyStore.class);

  /**
   * Gets an instance of the TrainingKeyStore
   * 
   * @return
   */
  public static TrainingKeyStore getInstance() {
    return _trainingKeyStore;
  }

  /**
   * Private constructor for initialising a TrainingKeyStore
   */
  private TrainingKeyStore() {

    try {
      keyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      // We used a fixed path because this will be running on Android
      keyStore.load("/sdcard/trainingmode/trainingKeystore.bks", "".toCharArray());

    }
    catch (KeyStoreException e) {
      logger.error("Exception loading training mode keystore", e);
    }

  }

  /**
   * Get the VPS Public Key
   * 
   * @return BLSPublicKey for the training mode VPS device
   * @throws TVSKeyStoreException
   */
  public BLSPublicKey getVPSPublicKey() throws TVSKeyStoreException {
    return keyStore.getBLSPublicKey("VPS");
  }

  /**
   * Gets the MBB Public Key
   * 
   * @return BLSPublicKey for the dummy MBB
   * @throws TVSKeyStoreException
   */
  public BLSPublicKey getMBBPublicKey() throws TVSKeyStoreException {
    return keyStore.getBLSPublicKey("MBB");
  }

  /**
   * Get the EVM Public Key
   * 
   * @return BLSPublicKey for the training mode EVM device
   * @throws TVSKeyStoreException
   */
  public BLSPublicKey getEVMPublicKey() throws TVSKeyStoreException {
    return keyStore.getBLSPublicKey("EVM");
  }

  /**
   * Get the VPS Private Key
   * 
   * @return BLSPrivateKey for the training mode VPS
   * @throws TVSKeyStoreException
   */
  public BLSPrivateKey getVPSPrivateKey() throws TVSKeyStoreException {
    return keyStore.getBLSPrivateKey("VPS");
  }

  /**
   * Get the MBB Private Key
   * 
   * @return BLSPrivateKey for the dummy MBB
   * @throws TVSKeyStoreException
   */
  public BLSPrivateKey getMBBPrivateKey() throws TVSKeyStoreException {
    return keyStore.getBLSPrivateKey("MBB");
  }

  /**
   * Get the EVM Private Key
   * 
   * @return BLSPrivateKey for the training mode EVM device
   * @throws TVSKeyStoreException
   */
  public BLSPrivateKey getEVMPrivateKey() throws TVSKeyStoreException {
    return keyStore.getBLSPrivateKey("EVM");
  }

  /**
   * Get the Cancel Authorisation Private Key
   * 
   * @return BLSPrivateKey for the training mode Cancel authorisation service
   * @throws TVSKeyStoreException
   */
  public BLSPrivateKey getCancelAuthPrivateKey() throws TVSKeyStoreException {
    return keyStore.getBLSPrivateKey("CancelAuth");
  }
}
