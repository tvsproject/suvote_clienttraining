/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyException;
import uk.ac.surrey.cs.tvs.client.TrainingClient;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.VVoteWebServer;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * A simple Proxy Server for receiving messages from the front-end HTML and preparing them for forwarding to the peered MBB. It then
 * awaits the response, checks the signatures and responds to the front-end client. It can run three different servlets for
 * different tasks, it is unlikely all three would normally be run on a single tablet. Generally, only POD/Audit/Cancel servlets
 * will be run on admin/pod tablets. Whilst the EVM tablet will only run the MBBProxy for vote submission.
 * 
 * @author Chris Culnane
 * 
 */
public class ProxyServer {

  /**
   * Holds the reference to the SimpleWebServer
   */
  private VVoteWebServer               sws         = null;

  /**
   * HashMap of Servlets and their paths
   */
  private HashMap<String, NanoServlet> servlets    = new HashMap<String, NanoServlet>();

  /**
   * Logger
   */
  private static final Logger          logger      = LoggerFactory.getLogger(ProxyServer.class);

  /**
   * Schema ID field.
   */
  private static final String          SCHEMA_ID   = "id";

  /**
   * Schema file field.
   */
  private static final String          SCHEMA_FILE = "schemaFile";

  /**
   * Create a new Proxy server and construct servlets for the different operations.
   * 
   * @param clientConfFile
   *          String path to the client config file
   * @param districtConfFile
   *          String path to the district config file
   * @throws JSONIOException
   *           if loading a config fails
   * @throws PeerSSLInitException
   * @throws IOException
   * @throws MaxTimeoutExceeded
   * @throws CryptoIOException
   * @throws NoSuchAlgorithmException 
   */
  public ProxyServer(String clientConfFile, String districtConfFile) throws JSONIOException, IOException, PeerSSLInitException,
      MaxTimeoutExceeded, CryptoIOException, NoSuchAlgorithmException {
    super();

    CryptoUtils.initProvider();
    logger.info("Creating proxy server");
    TrainingClient client = new TrainingClient(clientConfFile);

    switch (client.getType()) {
      case CANCEL:
        //this.servlets.put("MBBPODProxy", new MBBPODProxyServlet(client, districtConfFile));
        logger.info("Added MBBPODProxy Servlet");
        break;
      case EVM:
        this.servlets.put("MBBProxy", new MBBProxyServlet(client.getRaceDefs()));
        logger.info("Added MBBProxy Servlet");
        break;
      case VPS:
        this.servlets.put("MBBPODProxy", new MBBPODProxyServlet(districtConfFile,client.getRaceDefs()));
        logger.info("Added MBBPODProxy Servlet");
        this.servlets.put("PODProxy", new PODProxyServlet(client, districtConfFile));
        this.servlets.put("CancelProxy", new CancelAuthProxyServlet());
        logger.info("Added PODProxy Servlet");
        break;
      default:
        break;
    }
  }

  /**
   * Adds a servlet to the list of servlets - primarily used for the Android interface to allow a single server to run the Print and
   * Barcode servlets as well.
   * 
   * @param servletName
   *          String servlet name - how it will be called
   * @param servlet
   *          NanoServlet to call
   */
  public void addServlet(String servletName, NanoServlet servlet) {
    this.servlets.put(servletName, servlet);
  }

  /**
   * Starts the server on the specified port, using the specified directory as the root server folder.
   * 
   * @param port
   *          integer port number to listen on
   * @param root
   *          File directory of root folder
   * @throws IOException
   *           thrown when exception occur during start
   * @throws ProxyException
   *           thrown if you try and start an already running server
   */
  public void startServer(int port, File root) throws IOException, ProxyException {
    logger.info("Starting proxy server on {} with root at {}", port, root);

    if (this.sws == null) {
      this.sws = new VVoteWebServer("localhost", port, root, this.servlets);
      this.sws.start();

      logger.info("Started Proxy Server");
    }
    else {
      throw new ProxyException("Server is already running");
    }
  }

  /**
   * Stops the server and resets the sws variable to null.
   */
  public void stopServer() {
    if (this.sws != null) {
      logger.info("Stopping proxy server");
      this.sws.stop();
      this.sws = null;
    }
  }

  /**
   * Gets the a boolean value determining if the server was started and is still alive
   * 
   * @return boolean true if server is still alive, false if not
   */
  public boolean isRunning() {
    if (sws != null) {
      return this.sws.isAlive();
    }
    else {
      return false;
    }
  }

  /**
   * Constructs an Error Message from the JSONArray responses and a string message.
   * 
   * @param responses
   *          JSONArray of MBB responses
   * @param errorMessage
   *          String message providing further details of the error
   * @return JSONObject representing the response message
   * @throws JSONException
   */
  public static JSONObject constructErrorMessage(JSONArray responses, String errorMessage) throws JSONException {
    JSONObject msg = new JSONObject();
    msg.put(ClientConstants.UIERROR.ERROR, errorMessage);
    msg.put(ClientConstants.UIERROR.WBB_RESPONSES, responses);

    return msg;
  }

  /**
   * Constructs an Error Message with a string message - use this if the error occurs before communicating with the MBB
   * 
   * @param errorMessage
   *          String message providing further details of the error
   * @return JSONObject representing the response message
   * @throws JSONException
   */
  public static JSONObject constructErrorMessage(String errorMessage) throws JSONException {
    JSONObject msg = new JSONObject();
    msg.put(ClientConstants.UIERROR.ERROR, errorMessage);
    return msg;
  }

  /**
   * Static method to load schemas into a HashMap<String,String> to provide cached access to them for processing when messages come
   * in.
   * 
   * @param schemaList
   *          String path to the file containing a JSONArray of schemas to load
   * @return HashMap<String,String> containing schemas indexed by SCHEMA_ID
   */
  public static HashMap<String, String> loadSchemas(String schemaList) {
    HashMap<String, String> schemas = new HashMap<String, String>();
    try {
      JSONArray schemaArray = IOUtils.readJSONArrayFromFile(schemaList);
      for (int i = 0; i < schemaArray.length(); i++) {
        JSONObject schema = schemaArray.getJSONObject(i);
        try {
          schemas.put(schema.getString(SCHEMA_ID), IOUtils.readStringFromFile(schema.getString(SCHEMA_FILE)));
        }
        catch (IOException e) {
          logger.error("Exception whilst loading schema {}", schema.getString(SCHEMA_FILE), e);
        }
      }

    }
    catch (JSONIOException eio) {
      logger.error("Exception whilst loading schema list file  {}", schemaList, eio);
    }
    catch (JSONException eio) {
      logger.error("Exception whilst loading schema list file  {}", schemaList, eio);
    }
    return schemas;
  }

  /**
   * Main entry point to the server when being started from the CLI. Normally this would be started by a wrapper class, for example,
   * an Android service.
   * 
   * This will run until "stop" is typed into the console and enter is pressed.
   * 
   * @param args
   *          Command line arguments. String array of 3 arguments {ListeningPort, ServerRoot, ClientConfig path}
   * @throws IOException
   */
  public static void main(String[] args) {
    try {
      if (args.length == 3) {
        ProxyServer proxy = new ProxyServer(args[2], "districtconf.json");
        proxy.startServer(Integer.parseInt(args[0]), new File(args[1]));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (!br.readLine().equals("stop")) {
          // Do nothing.
        }
      }
      else {
        printUsage();
      }
    }
    catch (JSONIOException e) {
      logger.error("Couldn't start proxy server", e);
      System.err.println("Could not start server");
      System.exit(1);
    }
    catch (ProxyException e) {
      logger.error("Couldn't start proxy server", e);
      System.err.println("Could not start server");
      System.exit(1);
    }
    catch (IOException e) {
      logger.error("Couldn't start proxy server", e);
      System.err.println("Could not start server");
      System.exit(1);
    }
    catch (PeerSSLInitException e) {
      logger.error("Couldn't start proxy server", e);
      System.err.println("Could not start server");
      System.exit(1);
    }
    catch (MaxTimeoutExceeded e) {
      logger.error("Couldn't start proxy server", e);
      System.err.println("Could not start server");
      System.exit(1);
    }
    catch (CryptoIOException e) {
      logger.error("Couldn't start proxy server", e);
      System.err.println("Could not start server");
      System.exit(1);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Couldn't start proxy server", e);
      System.err.println("Could not start server");
      System.exit(1);
    }
  }

  /**
   * Prints the usage instructions to the System.out
   */
  private static final void printUsage() {
    System.out.println("Usage of ProxyServer:");
    System.out.println("ProxyServers starts the web server that will serve");
    System.out.println("the servlets used by the front-end. If started from");
    System.out.println("the CLI it will run until \"stop\" is entered on the CLI");
    System.out.println("Usage:\n");
    System.out.println("ProxyServer listenPort serverRoot pathToConfigFile\n");
    System.out.println("\tlistenPort: port for the server to listen on.");
    System.out.println("\tserverRoot: root folder to serve.");
    System.out.println("\tpathToConfigFile: path to configuration file.");

  }
}
