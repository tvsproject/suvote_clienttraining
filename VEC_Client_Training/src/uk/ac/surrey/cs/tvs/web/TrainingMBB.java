/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.AuditMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.PODMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.StartEVMMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;

/**
 * Dummy MBB to be used during TrainingMode. Acts like a standard MBB but runs locally using the dummy keys generated and stored in
 * the TrainingKeyStore
 * 
 * @author Chris Culnane
 * 
 */
public class TrainingMBB {

  /**
   * Logger
   */
  private static final Logger           logger      = LoggerFactory.getLogger(TrainingMBB.class);
  /**
   * Singleton instance of training MBB
   */
  private static final TrainingMBB      _instance   = new TrainingMBB();

  /**
   * HashMap to record the current state of serial numbers - i.e. how they have been used
   */
  private HashMap<String, MessageState> serialState = new HashMap<String, MessageState>();

  /**
   * Private constructor
   */
  private TrainingMBB() {

  }

  /**
   * Gets an instance of the TrainingMBB
   * 
   * @return TrainingMBB instance
   */
  public static TrainingMBB getInstance() {
    return _instance;
  }

  /**
   * Signs an incoming Print on Demand message
   * 
   * @param msg
   *          JSONObject containing incoming Print on Demand request
   * @return String containing Base64 signature or "ERROR" if an exception occurs
   */
  public String signPODMessage(JSONObject msg) {
    try {
      serialState.put(msg.getString(PODMessage.ID), MessageState.POD);
      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
      signature.update(msg.getString(PODMessage.ID));
      signature.update(msg.getString(PODMessage.DISTRICT));
      return signature.signAndEncode(EncodingType.BASE64);

    }
    catch (TVSKeyStoreException e) {
      logger.error("Exception during signing POD Message in Training mode", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception during signing POD Message in Training mode", e);
    }
    catch (JSONException e) {
      logger.error("Exception during signing POD Message in Training mode", e);
    }
    return "ERROR";
  }

  /**
   * Signs a cancel authorisation message in Training mode
   * 
   * @param msg
   *          JSONObject containing cancel authorisation request
   * @return String containing Base64 signature or "ERROR" if an exception occurs
   */
  public String signCancelAuthMessage(JSONObject msg) {
    try {
      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getCancelAuthPrivateKey());
      signature.update(msg.getString(PODMessage.ID));
      signature.update(msg.getString(PODMessage.DISTRICT));
      return signature.signAndEncode(EncodingType.BASE64);

    }
    catch (TVSKeyStoreException e) {
      logger.error("Exception during signing Cancel Auth in Training mode", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception during signing Cancel Auth in Training mode", e);
    }
    catch (JSONException e) {
      logger.error("Exception during signing Cancel Auth in Training mode", e);
    }
    return "ERROR";
  }

  /**
   * Signs a StartEVM message in Training mode
   * 
   * @param msg
   *          JSONObject containing StartEVM request
   * @return String containing Base64 signature or "ERROR" if an exception occurs
   */
  public String signStartEVMMessage(JSONObject msg) {
    try {
      serialState.put(msg.getString(PODMessage.ID), MessageState.STARTEVM);

      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
      signature.update("startevm");

      signature.update(msg.getString(StartEVMMessage.ID));
      signature.update(msg.getString(StartEVMMessage.DISTRICT));
      return signature.signAndEncode(EncodingType.BASE64);
    }
    catch (TVSKeyStoreException e) {
      logger.error("Exception during signing StartEVM in Training mode", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception during signing StartEVM in Training mode", e);
    }
    catch (JSONException e) {
      logger.error("Exception during signing StartEVM in Training mode", e);
    }
    return "ERROR";
  }

  /**
   * Signs a cancel message in Training mode
   * 
   * @param msg
   *          JSONObject containing Cancel request
   * @return String containing Base64 signature or "ERROR" if an exception occurs
   */
  public String signCancelMessage(JSONObject msg) {
    try {
      serialState.put(msg.getString(PODMessage.ID), MessageState.CANCEL);

      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
      signature.update("cancel");

      signature.update(msg.getString(StartEVMMessage.ID));
      return signature.signAndEncode(EncodingType.BASE64);
    }
    catch (TVSKeyStoreException e) {
      logger.error("Exception during signing Cancel in Training mode", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception during signing Cancel in Training mode", e);
    }
    catch (JSONException e) {
      logger.error("Exception during signing Cancel in Training mode", e);
    }
    return "ERROR";
  }

  /**
   * Signs a audit message in Training mode
   * 
   * @param msg
   *          JSONObject containing audit request
   * @param ballotReductions
   *          JSONArray of permutation after ballot reductions have been performed
   * @return String containing Base64 signature or "ERROR" if an exception occurs
   * @throws TVSKeyStoreException
   * @throws TVSSignatureException
   * @throws JSONException
   */
  public String signAuditMessage(JSONObject msg, JSONArray ballotReductions) throws TVSKeyStoreException, TVSSignatureException,
      JSONException {
    serialState.put(msg.getString(PODMessage.ID), MessageState.POD);
    TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
    signature.update("audit");
    signature.update(msg.getString(AuditMessage.ID));
    StringBuffer reducedPerms = new StringBuffer();

    for (int raceCount = 0; raceCount < ballotReductions.length(); raceCount++) {
      reducedPerms.append(ballotReductions.getJSONArray(raceCount).join(MessageFields.PREFERENCE_SEPARATOR));
      reducedPerms.append(MessageFields.RACE_SEPARATOR);
    }
    signature.update(reducedPerms.toString());
    return signature.signAndEncode(EncodingType.BASE64);

  }

  /**
   * Checks if a serial number has already been used. This is session dependent, the state of serial numbers is not persistent and
   * will reset after a restart
   * 
   * @param serialNo
   *          String serial number to check
   * @return boolean true if it has already been used, false if not
   */
  public boolean serialNoUsed(String serialNo) {
    return this.serialState.containsKey(serialNo);
  }

  /**
   * Gets the current state of a serial number
   * 
   * @param serialNo
   *          String serial number to get state for
   * @return MessageState of the current state of specified serial number
   */
  public MessageState serialNoState(String serialNo) {
    MessageState returnState = MessageState.UNUSED;
    if (this.serialState.containsKey(serialNo)) {
      returnState = this.serialState.get(serialNo);
    }
    return returnState;
  }
}
