/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.prng.FixedSecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandomBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIPODMessage;

/**
 * Dummy TrainingMode Ballot database. Training mode works slightly differently to normal, in that you can reuse ballots to aid
 * training (where you may pre-print candidate lists and have many EVMs to concurrently train multiple people
 * 
 * @author Chris Culnane
 * 
 */
public class TrainingBallotDB {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(TrainingBallotDB.class);

  /**
   * JSONObject that contains the district config
   */
  private JSONObject          districtConf;

  /**
   * Instantiate the TrainingBallotDB with the appropriate District config
   * 
   * @param districtConf
   *          JSONObject containing the district config
   * @throws NoSuchAlgorithmException
   */
  public TrainingBallotDB(JSONObject districtConf) throws NoSuchAlgorithmException {
    this.districtConf = districtConf;
  }

  /**
   * Generates and returns a ballot with the specified serial number for the specified district. The permutation is generated as a
   * function of the serial number, as such, requesting the same serial number twice will result in the same ballot being generated.
   * This has the advantage of providing repeatability as well as not requiring a ballot generation stage to be performed, since
   * they are generated on demand.
   * 
   * @param serialNo
   *          String serial number to use for the ballot and generation
   * @param district
   *          String district for the ballot
   * @return JSONObject containing the permutation and ballot information
   * @throws NoSuchAlgorithmException
   */
  public JSONObject getBallot(String serialNo, String district) throws NoSuchAlgorithmException {
    try {

      // Use DRBG from BouncyCastle to prepare source of randomness from hash output
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < 10; i++) {
        sb.append(serialNo);
      }
      FixedSecureRandom fsr = new FixedSecureRandom(sb.toString().getBytes());
      // We disable predictionResistant since this would require re-seeding and we only have a finite amount of seed randomness
      SP800SecureRandomBuilder rBuild = new SP800SecureRandomBuilder(fsr, false);
      SP800SecureRandom generatePermutation = rBuild.buildHash(new SHA256Digest(), null, false);

      JSONObject districtDetails = districtConf.getJSONObject(district);
      ArrayList<ArrayList<Integer>> permutation = this.generateBaseBallotAndPermute(districtDetails, generatePermutation);
      JSONObject ballot = new JSONObject();
      ballot.put(UIPODMessage.SERIAL_NO, serialNo);
      ballot.put(UIPODMessage.DISTRICT, district);
      // ballot.put("sigs", "SomeSignature");
      JSONArray races = new JSONArray();
      JSONObject la = new JSONObject();
      la.put("id", "LA");
      la.put("permutation", permutation.get(0));
      races.put(la);
      JSONObject atl = new JSONObject();
      atl.put("id", "LC_ATL");
      atl.put("permutation", permutation.get(1));
      races.put(atl);
      JSONObject btl = new JSONObject();
      btl.put("id", "LC_BTL");
      btl.put("permutation", permutation.get(2));
      races.put(btl);
      ballot.put("races", races);
      return ballot;
    }
    catch (JSONException e) {
      logger.error("Exception whilst generating training mode ballot", e);
    }
    return null;
  }

  /**
   * Performs the actual permutation generation for a particular district
   * 
   * @param district
   *          JSONObject with district sizes
   * @param permGenerator
   *          SecureRandom permutation generator based on DRBG
   * @return ArrayList of ArrayLists, one per race, containing the permutation indexes
   * @throws JSONException
   */
  public ArrayList<ArrayList<Integer>> generateBaseBallotAndPermute(JSONObject district, SecureRandom permGenerator)
      throws JSONException {
    ArrayList<ArrayList<Integer>> perms = new ArrayList<ArrayList<Integer>>();
    for (int raceCount = 0; raceCount < 3; raceCount++) {

      ArrayList<Integer> racePerm = new ArrayList<Integer>();
      String raceString = "";
      switch (raceCount) {
        case 0:
          raceString = "la";
          break;
        case 1:
          raceString = "lc_atl";
          break;
        case 2:
          raceString = "lc_btl";
          break;
      }
      for (int i = 0; i < district.getInt(raceString); i++) {
        racePerm.add(i);
      }
      Collections.shuffle(racePerm, permGenerator);
      perms.add(racePerm);
    }
    return perms;

  }

}
