/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

/**
 * RemovedCipher is a wrapper class to keep track of the ciphers that are removed as part of ballot reduction. We need to map
 * between permuted index and candidate index.
 * 
 * @author Chris Culnane
 * 
 */
public class RemovedCipher implements Comparable<RemovedCipher> {

  /**
   * int index is the permuted index
   */
  private int index;

  /**
   * int candidateIndex is the candidate index in the original base list of candidates
   */
  private int candidateIndex;

  /**
   * Constructs a new RemovedCipher with an index and candidateIndex
   * 
   * @param index
   *          int of the permuted index
   * @param candidateIndex
   *          int of the original candidate index
   */
  public RemovedCipher(int index, int candidateIndex) {
    super();

    this.index = index;
    this.candidateIndex = candidateIndex;
  }

  /**
   * Compares the removed ciphers based on the permuted index. This is used to sort the RemovedCiphers prior to removing them from
   * the actual array
   * 
   * @param o
   *          RemovedCipher to compare this to
   * @return int 0 if equal, >0 if greater <0 if less than
   * 
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(RemovedCipher o) {
    return this.index - o.getIndex();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof RemovedCipher))
      return false;
    return compareTo((RemovedCipher) obj) == 0;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 37;
    int result = 1;
    result = prime * result + index;
    return result;
  }
  /**
   * Gets the original candidate index
   * 
   * @return int original candidate index
   */
  public int getCandidateIndex() {
    return this.candidateIndex;
  }

  /**
   * Gets the permuted index
   * 
   * @return int permuted index
   */
  public int getIndex() {
    return this.index;
  }
}
