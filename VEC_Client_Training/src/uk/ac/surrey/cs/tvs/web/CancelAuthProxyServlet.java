/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle ballot reduction.
 * 
 * @author Chris Culnane
 * 
 * @version $Revision: 1.0 $
 */
public class CancelAuthProxyServlet implements NanoServlet {
  
  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(CancelAuthProxyServlet.class);

  /**
   * String to hold the schema in
   */
  private String              schema;

  /**
   * Path to the schema that covers messages received by this proxy
   */
  private static String       SCHEMA_PATH = "/sdcard/trainingmode/schemas/cancelauthproxyschema.json";

  /**
   * Creates a new PODProxyServlet that references the configFile and districtConfig as specified. This will handle Print on Demand
   * requests from the client HTML.
   * 
   * 
   * @param districtConfFile
   *          path to district configuration file - number of candidates in each race
   * @param client
   *          Client
   * @throws JSONIOException
   * @throws CryptoIOException
   */
  public CancelAuthProxyServlet() throws JSONIOException, CryptoIOException {
    super();

    logger.info("Loading CancelAuthProxyServlet");
    
    this.schema = JSONUtils.loadSchema(CancelAuthProxyServlet.SCHEMA_PATH);
    logger.info("Finished loading CancelAuthProxyServlet");
  }

  /**
   * Process and incoming message string, which should be in JSON format and with the relevant fields.
   * 
   * @param message
   *          JSON String of a message
   * 
   * 
   * @return JSONObject response
   * @throws ProxyException
   */
  private JSONObject processMessage(String message) throws ProxyException {
    try {
      // We know the type is pod because the schema restricts it

      // Load the message object
      JSONObject messageSent = new JSONObject(message);

      // Add the data to the messageSent - this will be the message that is then sent onto the MBB peers
      // Prepare to sign the contents of the message
      JSONObject auth = new JSONObject();
      
      auth.put("serialNo", messageSent.getString("serialNo"));
      auth.put("type", messageSent.getString("type"));
      auth.put("cancelAuthID", "CancelAuth");
      auth.put("cancelAuthSig", TrainingMBB.getInstance().signCancelAuthMessage(messageSent));
      return auth;

    }
    catch (JSONException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
    
  }

  
  /**
   * Runs the servlet.
   * 
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        if (JSONUtils.validateSchema(this.schema, params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, this.processMessage(
              params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }
      catch (ProxyException e) {
        logger.error("Exception whilst processing message {}", params.get(ClientConstants.REQUEST_MESSAGE_PARAM), e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
    }
    else {
      logger.warn("Missing msg parameter from request");
      return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT,
          "Missing msg parameter - no message to process");
    }
  }
  
  


}
