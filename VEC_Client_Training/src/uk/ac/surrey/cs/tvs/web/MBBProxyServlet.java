/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIResponse;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDefs;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.utils.verify.PrefsVerification;
import uk.ac.surrey.cs.tvs.utils.verify.VerifyPreferences;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle voting.
 * 
 * @author Chris Culnane
 * 
 * @version $Revision: 1.0 $
 */
public class MBBProxyServlet implements NanoServlet {

  /**
   * Logger
   */
  private static final Logger     logger            = LoggerFactory.getLogger(MBBProxyServlet.class);

  /**
   * Path to the schema file
   */
  private static final String     SCHEMA_LIST_PATH  = "/sdcard/trainingmode/schemas/mbbproxyschemalist.json";

  /**
   * Constant string to refer to the general schema
   */
  private static final String     GENERAL_SCHEMA_ID = "general";


  /**
   * HashMap that holsd a list of schemas indexed by type
   */
  private HashMap<String, String> schemas;

  private RaceDefs raceDefs;

  /**
   * Constructor requiring the configuration file.
   * 
   * @param client
   *          Client that this servlet interacts with
   * 
   * @throws JSONIOException
   * @throws CryptoIOException
   */
  public MBBProxyServlet(RaceDefs raceDefs) throws JSONIOException, CryptoIOException {
    super();
    this.raceDefs=raceDefs;
    logger.info("Loading MBBProxyServlet");

    this.schemas = ProxyServer.loadSchemas(MBBProxyServlet.SCHEMA_LIST_PATH);

    logger.info("Finished loading MBBProxyServlet");
  }

  /**
   * Processes a servlet message.
   * 
   * @param message
   *          The message to process.
   * 
   * 
   * @return The message that has been sent to the MBB in response to the servlet request.
   * @throws JSONException
   * @throws IOException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws UnrecoverableKeyException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws MBBCommunicationException
   * @throws ProxyMessageProcessingException
   * @throws ConsensusException
   */
  private JSONObject processMessage(String message) throws JSONException, IOException, KeyStoreException, NoSuchAlgorithmException,
      CertificateException, UnrecoverableKeyException, InvalidKeyException, SignatureException, CryptoIOException,
      TVSSignatureException, JSONIOException, MBBCommunicationException, ProxyMessageProcessingException, ConsensusException {

    JSONObject messageSent = new JSONObject(message);

    if (VerifyPreferences.checkAllRacePreferences(messageSent.getJSONArray(ClientConstants.UIVOTEMessage.RACES),this.raceDefs) != PrefsVerification.ALL_VALID) {
      logger.error("Preferences are invalid");
      throw new ProxyMessageProcessingException("Preferences are invalid");
    }
    String commitTime = "1390759200000";

    logger.info("Signature on response verified");
    messageSent.put(ClientConstants.UIVOTEResponse.COMMIT_TIME, commitTime);
    messageSent.remove(JSONWBBMessage.TYPE);
    messageSent.put(UIResponse.WBB_SIGNATURES, "WBBSig");

    return messageSent;
  }

  /**
   * Processes a StartEVM message that is sent when the voting session begins
   * 
   * @param message
   *          String representation of JSON message object
   * 
   * 
   * @return JSONObject with the WBB authorisation signatures
   * @throws JSONException
   * @throws IOException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws UnrecoverableKeyException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws MBBCommunicationException
   * @throws ProxyMessageProcessingException
   * @throws ConsensusException
   */
  private JSONObject processStartEVMMessage(String message) throws JSONException, IOException, KeyStoreException,
      NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, InvalidKeyException, SignatureException,
      CryptoIOException, TVSSignatureException, JSONIOException, MBBCommunicationException, ProxyMessageProcessingException,
      ConsensusException {
    JSONObject messageSent = new JSONObject(message);

    messageSent.remove(JSONWBBMessage.TYPE);
    messageSent.put(UIResponse.WBB_SIGNATURES, TrainingMBB.getInstance().signStartEVMMessage(messageSent));

    return messageSent;
  }

  /**
   * Runs the servlet.
   * 
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        if (!JSONUtils.validateSchema(this.schemas.get(GENERAL_SCHEMA_ID), params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
        JSONObject msg = new JSONObject(params.get(ClientConstants.REQUEST_MESSAGE_PARAM));
        if (JSONUtils.validateSchema(this.schemas.get(msg.get(ClientConstants.UIMessage.TYPE)),
            params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          if (msg.get(ClientConstants.UIMessage.TYPE).equals("startevm")) {
            return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, this.processStartEVMMessage(
                params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
          }
          else if (msg.get(ClientConstants.UIMessage.TYPE).equals("vote")) {
            return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, this.processMessage(
                params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
          }
          else {
            return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Unknown message type");
          }
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }
      catch (UnrecoverableKeyException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (InvalidKeyException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (KeyStoreException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (NoSuchAlgorithmException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (CertificateException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (SignatureException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (JSONException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (IOException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (CryptoIOException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (TVSSignatureException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (JSONIOException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (MBBCommunicationException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (ProxyMessageProcessingException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (ConsensusException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
    }
    else {
      return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT,
          "Missing msg parameter - no message to process");
    }
  }
}
